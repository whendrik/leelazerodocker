# Leela Zero - Dockerfile & Run Instructions for Sabaki

You need to install docker

https://docs.docker.com/install/

## Create Image with leelaz_dockerfile.txt

To make the process of updated network more easy, a network-less version of leela zero is build & used to ultimately build a network including version on top. 
The network including version can quickly be re-build to keep the network up to date.

```
docker build -t leelazeronetworkless:latest --no-cache -f leelazero_dockerfile_networkless.txt .
```

```
docker build -t leelazero:latest --no-cache -f leelazero_dockerfile.txt .
```

## Configure Sabaki to use Leela-Zero from docker

The following networks are downloaded:

- `best-network`
- `20-blocks`
- `15-blocks`
- `10-blocks`
- `6-blocks`
- `5-blocks`

Say you want to play against `15-blocks`, 
Configure Sabaki at
`Engines` > `Manage Engines` and use the following settings; 

```
docker
run -i --rm leelazero -g --noponder --weights "15-blocks"
time_settings 0 30 1;
```

Replace `15-blocks` with your network of choice, and the `time_settings 0 30 1;` are optional.

Also optional, you can limit the number of playouts or visits
```
-p [ --playouts ] arg		Weaken engine by limiting the number of playouts. Requires --noponder.
-v [ --visits ] arg			Weaken engine by limiting the number of visits.
```
e.g.

```
docker
run -i --rm leelazero -g --noponder -v 10 --weights "15-blocks"
time_settings 0 30 1;
```

![sabaki](images/sabaki.png "Sabaki")

## Lizzie

Create dockerfile, based on `consol/ubuntu-xfce-vnc` (Ubuntu + VNC), 

Lizzie comes with a 15 block network
```
docker build --no-cache -t lizzie:latest -f lizzie_dockerfile_default.txt .
```

Optionally, create a 10 blocks version
```
docker build --no-cache -t lizzie:10blocks -f lizzie_dockerfile.txt .
```

Modify the dockerfile and build command to add other networks.

Run
```
docker run --rm -d -p 5901:5901 -p 6901:6901 lizzie:10blocks
```
and

vnc to port `vnc://127.0.0.1:5901`, password `vncpassword` -or-
browse to `http://127.0.0.1:6901/`, password `vncpassword`
